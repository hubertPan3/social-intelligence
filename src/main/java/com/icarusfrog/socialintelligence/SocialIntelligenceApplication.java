package com.icarusfrog.socialintelligence;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SocialIntelligenceApplication {

	public static void main(String[] args) {
		SpringApplication.run(SocialIntelligenceApplication.class, args);
	}

}
